﻿namespace web_api.Controllers

open Microsoft.AspNetCore.Mvc

[<Route("api/[controller]")>]
[<ApiController>]
type HelloController () =
    inherit ControllerBase()

    [<HttpGet>]
    member this.Get() =
        ActionResult<string>("Hello World")

    [<HttpGet("{name}")>]
    member this.Get(name:string) =
        let value = sprintf "Hello %s" name
        ActionResult<string>(value)

    [<HttpPost>]
    member this.Post([<FromBody>] value:string) =
        ()

    [<HttpPut("{name}")>]
    member this.Put(name:string, [<FromBody>] value:string ) =
        ()

    [<HttpDelete("{name}")>]
    member this.Delete(name:string) =
        ()
